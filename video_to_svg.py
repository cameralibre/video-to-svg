import os
import sys
import re
import subprocess
import json
from bs4 import BeautifulSoup

# methods
def numeric(string):
    if '/' in string:
        fraction = string.split('/')
        number = float(fraction[0]) / float(fraction[1])
    else:
        number = float(string)
    return number

def get_video_info(video):
    ffprobe_command = f'ffprobe {video} -v quiet -print_format json -show_format -show_streams'
    ffprobe_output = json.loads(subprocess.check_output(ffprobe_command, shell=True).decode('utf-8'))
    video_info = {
        'width': ffprobe_output['streams'][0]['width'],
        'height': ffprobe_output['streams'][0]['height'],
        'duration': ffprobe_output['format']['duration'],
        'framerate': numeric(ffprobe_output['streams'][0]['r_frame_rate'])
    }
    return video_info

def create_image_sequence(video, framerate):
    os.mkdir(frames_directory)
    ffmpeg_split_video_cmd = f'ffmpeg -hide_banner -loglevel error -i {video} -r {framerate} {frames_directory}/frame-%04d.png'
    os.system(ffmpeg_split_video_cmd)
    return sorted(os.listdir(frames_directory))

def add_y_offset(index, frame):
    with open(f'{frame}.svg', 'r') as file:
        data = file.read().replace('\n', '')
        frame = BeautifulSoup(data, 'xml')
        frame.svg['transform'] = f'translate(0, {index * video_height})'
        frame.svg['overflow'] = 'hidden'
        return(frame.svg)

def convert_and_offset_svg(index, png_frame, shapes_per_frame):
    frame_name = png_frame.split('.')[0]
    print(f'Converting {frame_name}...')
    os.system(f'sqip -o {frame_name}.svg -n {shapes_per_frame} -b 0 {png_frame}')
    return add_y_offset(index, frame_name)

# parse conversion parameters and get video info using ffprobe (https://ffmpeg.org/ffprobe.html)
input_video = str(sys.argv[1])
video_name = input_video.split('.')[0]
video_width, video_height, video_duration, input_framerate = get_video_info(input_video).values()
frame_reduction_factor = float(sys.argv[2]) if len(sys.argv) > 2 else 0.5
output_framerate = round(input_framerate*frame_reduction_factor)
frames_directory = os.path.join(os.getcwd(), f'{video_name}_frames')

# convert video to individual frames at desired framerate using FFmpeg (https://ffmpeg.org/)
print(f'Converting {input_video} ({input_framerate}fps) to {output_framerate}fps PNG sequence')
image_sequence = create_image_sequence(input_video, output_framerate)
total_frames = len(image_sequence)
print(f'{total_frames} frames extracted')

# create a parent SVG containing a group called #strip
# this will be the 'filmstrip' of SVG frames that is animated vertically through the SVG's viewBox
parent_svg =  BeautifulSoup(f'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 {video_width} {video_height}"><g id="strip" transform="translate(0,0)" overflow="hidden"></g></svg>', 'xml')

# convert each frame to simplified SVG using SQIP (https://github.com/axe312ger/sqip)
# add it to the filmstrip
shapes_per_frame = int(sys.argv[3]) if len(sys.argv) > 3 else 16
print(f'Converting each frame to an SVG made up of {shapes_per_frame} shapes')
os.chdir(frames_directory)
for index, png_frame in enumerate(image_sequence):
    svg_frame = convert_and_offset_svg(index, png_frame, shapes_per_frame)
    parent_svg.g.append(svg_frame)
os.chdir('..')

# add a <style> tag to the parent SVG, containing the css steps() animation,
# which will pull the 'filmstrip' upwards through the parent SVG's viewBox
animation = BeautifulSoup(f'<style>/* <![CDATA[ */#strip {{ animation: frame-by-frame {video_duration}s steps({total_frames}, jump-none) infinite; }} @keyframes frame-by-frame {{ 0% {{ transform: translate(0,0); }} 100% {{ transform: translate(0,-{video_height * (total_frames - 1)}px); }}}}/* ]]> */</style>', 'xml')
parent_svg.svg.insert(0, animation.style)

# tidy up
for file in os.listdir(frames_directory):
    os.remove(os.path.join(frames_directory, file))
os.rmdir(frames_directory)

with open(f'{video_name}.svg', "w", encoding = 'utf-8') as file:
    file.write(str(parent_svg.prettify()))

original_size = os.path.getsize(input_video)
svg_size = os.path.getsize(f'{video_name}.svg')
print(f'Converted!\n{input_video} ({input_framerate}fps, {original_size} bytes)\n{video_name}.svg ({output_framerate}fps, {svg_size} bytes)\n{round(100 - svg_size / original_size * 100, 2)}% reduction in filesize')
